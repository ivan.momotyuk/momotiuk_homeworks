const images = Array.from(document.querySelectorAll('.image-to-show'));
const imagesWrapper = document.querySelector('.images-wrapper');
const stopButton = document.createElement('button');
const restartButton = document.createElement('button');

let currentIndex = 0;
let timer;

stopButton.textContent = 'Припинити';
restartButton.textContent = 'Відновити показ';
imagesWrapper.insertAdjacentElement('afterend', stopButton);
imagesWrapper.insertAdjacentElement('afterend', restartButton);
images[currentIndex].style.display = 'block';

images.map(item => {
    if(item !== images[0]) {
        item.style.display = 'none';
    }
});

function showNextImage() {
  images[currentIndex].style.display = 'none';
  currentIndex = (currentIndex + 1) % images.length;
  images[currentIndex].style.display = 'block';
}

function startSlider() {
  timer = setInterval(showNextImage, 3000);
}

function stopSlider() {
  clearInterval(timer);
}

startSlider();

stopButton.addEventListener('click', stopSlider);

restartButton.addEventListener('click', function () {
  images[currentIndex].style.display = 'none';
  currentIndex = 0;
  images[currentIndex].style.display = 'block';
  startSlider();
});
