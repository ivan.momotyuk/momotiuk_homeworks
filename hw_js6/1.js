const newUser = {};

function createNewUser() {
  const firstName = prompt("What is your first name?");
  const lastName = prompt("What is your last name?");
  const birthday = prompt("Enter your birth date (dd.mm.yyyy):");
  
  newUser.firstName = firstName.trim();
  newUser.lastName = lastName.trim();
  newUser.birthday = birthday.trim();
  
  return newUser;
}

newUser.getLogin = function() {
  const login = `${this.firstName.charAt(0).toLowerCase()}${this.lastName.toLowerCase()}`.trim();
  return login;
};

newUser.getAge = function() {
  const [day, month, year] = this.birthday.split('.');
  const birthDate = new Date(`${month}/${day}/${year}`);
  const ageInMilliseconds = Date.now() - birthDate.getTime();
  const ageInYears = ageInMilliseconds / (1000 * 60 * 60 * 24 * 365.25);
  const age = Math.floor(ageInYears);
  return age;
};

newUser.getPassword = function() {
  const password = `${this.firstName.charAt(0).toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.slice(-4)}`.trim();
  return password;
};

console.log(createNewUser());
console.log(newUser.getAge());
console.log(newUser.getPassword());