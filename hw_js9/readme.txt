1. Опишіть, як можна створити новий HTML тег на сторінці.
Для того, щоб на сторінці відобразився новий елемент з тегом, слід його створити та "повісити" його в бажане місце:
а) Новий тег створюється методом .createElement для документу. Наприклад, створюємо новий дів:
let newTag = document.createElement('div');
b) Додати можна за допомогою методів .append, .prepend, .insertAdjacentHTML, etc.;
document.body.append(newTag);
Можна змінювати вміст та атрибути створеного тега відповідними методами перш, ніж додавати його на сторінку. 
Наприклад:
newTag.innerHTML = `Hello, <span class="span"> World! </span>`

2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
Перший параметр insertAdjacentHTML вказує, куди саме вставлятиметься другий параметр. 
beforebegin вставляє другий параметр перед елементом, до якого застосовується метод, afterbefin - в початок,
beforeend - в кінець, afterend - після елементу.
наприклад:
** - можливі місця, куди буде вставлено <li>inserted text</li> зі скрипта нижче

<body>
<!-- **beforebegin -->
<ul class="ul">
    <li>afterbegin</li>
    <li>initial text</li>
<!-- <li>**beforeend</li> -->
    </ul>
<!-- **afterend -->
</body>
<script> 
let list = document.querySelector('.ul');
list.insertAdjacentHTML ('afterbegin', '<li>inserted text</li>');
</script>

3. Як можна видалити елемент зі сторінки?
Необхідно обрати бажаний елемент одним із методів (наприклад querySelector) і використати метод remove.
let tagToRemove = document.querySelector('#unnecessary');
tagToRemove.remove();
Якщо треба видалити дочірні елементи, слід використовувати метод .removeChild() аналогічним способом.
Щоб видалити вміст елемента, можна задати порожнє значення його .outerHTML:
tagToRemove.outerHTML = '';