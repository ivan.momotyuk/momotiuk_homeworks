function arrToList(arr, elem = document.body) {
    let list = document.createElement('ul');
    elem.append(list);
    let listItems = arr.map((item) => {
        let li = document.createElement('li');
        if (Array.isArray(item)) {
            arrToList(item, li);
        } else {
            li.innerText = item;
        }
        return li;

    });
    list.append(...listItems);

}

//oчистка сторінки (додав трошки креативу)
let clearButton = document.createElement('button');
clearButton.textContent = 'Clear the page?';
document.body.prepend(clearButton);
clearButton.addEventListener('click', countdown);

function countdown() {
    clearButton.remove();
    setTimeout(() => {
        while (document.body.firstChild) {
            document.body.firstChild.remove();
        }
    }, 3000)

    let remainingTime = 3;
    const countdownElem = document.createElement('p');
    countdownElem.innerHTML = `<b>The page will be cleared in ${remainingTime}</b>`;
    countdownElem.style = `font-size: 20px; color: blue`;
    document.body.prepend(countdownElem);
    const interval = setInterval(() => {
        remainingTime--;
        countdownElem.innerHTML = `<b>The page will be cleared in ${remainingTime}</b>`;

        document.body.prepend(countdownElem);
        if (remainingTime <= 0) {
            clearInterval(interval);
            countdownElem.remove();
        }
    }, 1000);
}

arrToList(["hello", ['hey', 'hi'], "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);

//далі перевіряю, чи працює функція з елементами крім дефолтного, для цього створив div з іншим бекграундом 
let newPlace = document.createElement('div');
newPlace.innerHTML = 'This is a new section to test the function';
newPlace.style = `background-color: turquoise; color: black`;
document.body.append(newPlace);
arrToList(["I", "Have", "A pen", "An apple", "Boom", "Apple pen"], newPlace)