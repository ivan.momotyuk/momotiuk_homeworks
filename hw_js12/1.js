let buttons = Array.from(document.querySelectorAll('.btn'));
let buttonsContent = buttons.map(item => item.innerHTML.toUpperCase());
document.addEventListener('keydown', keyboard);

function keyboard(event) {
    let key = event.key.toUpperCase();

    //у ТЗ точно не було вказано, чи повинні фарбуватися всі кнопки в синій якщо натиснута кнопка, 
    //якої нема в списку кнопом, але я вирішив додати ще одну перевірку. 
    //якщо натиснута кнопка, якої нема в списку, таким чином попередня кнопка не зникає, 
    //а лишається підсвіченою
    
    if (buttonsContent.includes(key)) {
        buttons.map(item => {
            if (key === item.innerText.toUpperCase()){
                item.style = 'background-color: blue';
            } else {item.style = 'background-color: black'};
        })
    } else return
   
}