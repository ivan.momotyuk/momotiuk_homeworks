let numOne = prompt("Enter a number");
let numTwo = prompt("Enter another number");
let oper = prompt("Enter the desired operator: '+', '-', '/', or '*'", '+');
oper = String(oper);


while (isNaN(numOne)) {
    numOne = prompt("The first value you entered is not a number. Enter again.", numOne);
}

while (isNaN(numTwo)) {
    numTwo = prompt("The second value you entered is not a number. Enter again.", numTwo);
}

while (oper != '+' && oper != '-' && oper != '*' && oper != '/') {
    oper = prompt("Unknown operator. Choose among '+', '-', '/', or '*' and enter again.", oper);
}

numOne = Number(numOne);
numTwo = Number(numTwo);

function count(numOne, numTwo) {
    let result;
    if (oper == "+") {
        result = numOne + numTwo;
    } else if (oper == "-") {
        result = numOne - numTwo;
    } else if (oper == "/") {
        result = numOne / numTwo;
    } else if (oper == "*") {
        result = numOne * numTwo;
    }

    return console.log(result);

}

count(numOne, numTwo);