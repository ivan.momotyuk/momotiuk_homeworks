const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.getElementById('root');
const ul = document.createElement('ul');
root.appendChild(ul);

books.forEach((i) => {
    const ulForEachBook = document.createElement('ul');
  
    try {
      if (!i.hasOwnProperty('author')) {
        throw new Error('No Author Identified');
      }
     
  
      if (!i.hasOwnProperty('name')) {
        throw new Error('No Name Identified');
      }
     
  
      if (!i.hasOwnProperty('price')) {
        throw new Error('No Price Identified');
      }
      const liAuthor = document.createElement('li');
      liAuthor.textContent = i.author;
      ulForEachBook.appendChild(liAuthor);
      const liName = document.createElement('li');
      liName.textContent = i.name;
      ulForEachBook.appendChild(liName);
      const liPrice = document.createElement('li');
      liPrice.textContent = i.price;
      ulForEachBook.appendChild(liPrice);
    } catch (error) {
      console.error(error.message);
    }
  
    root.appendChild(ulForEachBook);
  });
  




  