fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(res => res.json())
  .then(data => {
    const filmContainers = data.map(item => {
      const filmContainer = document.createElement('div');

      const title = document.createElement('h2');
      title.id = item.episodeId;
      title.innerHTML = `Episode ${item.episodeId}: ${item.name}`;
      filmContainer.appendChild(title);

      const charactersList = document.createElement('p');
      filmContainer.appendChild(charactersList);

      const summary = document.createElement('p');
      summary.innerHTML = item.openingCrawl;
      filmContainer.appendChild(summary);

      document.body.appendChild(filmContainer);

      const characterNames = [];

      item.characters.forEach(url => {
        fetch(url)
          .then(res => res.json())
          .then(character => {
            characterNames.push(character.name);
            if (characterNames.length === item.characters.length) {
              charactersList.innerHTML = characterNames.join(', ');
            }
          })
      });
    });
  })
