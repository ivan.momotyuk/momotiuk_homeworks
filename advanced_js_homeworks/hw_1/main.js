class Employee {
    constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }
  
    set name(value) {
      this._name = value;
    }
  
    set age(value) {
      this._age = value;
    }
  
    set salary(value) {
      this._salary = value;
    }
  
    get name() {
      return this._name;
    }
  
    get age() {
      return this._age;
    }
  
    get salary() {
      return '$' + this._salary;
    }
  }
  
  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this._lang = lang;
    }
  
    set lang(values) {
      this._lang = values;
    }
  
    get lang() {
      return this._lang;
    }
  
    get salary() {
      return '$' + this._salary * 3;
    }
  }
  
  const programmer1 = new Programmer('Ivan', 30, 2000, ['English', 'German', 'Spanish', 'Ukrainian']);
  const programmer2 = new Programmer('John', 25, 1500, ['Java', 'Python', 'C++']);
  
  console.log(programmer1);
  console.log(programmer2);
  console.log(programmer1.salary);
  console.log(programmer2.salary);
  