//Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let paragraphs = document.querySelectorAll('p');
for (let i = 0; i < paragraphs.length; i++) {
    paragraphs[i].style.background = '#FF0000'
};

//Знайти елемент із id="optionsList". Вивести у консоль. 
let optionsList = document.querySelector('#optionsList');
console.log(optionsList);

//Знайти батьківський елемент та вивести в консоль. 
let optionsParent = optionsList.parentNode;
console.log(optionsParent);

//Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод
let optionsChildren = optionsList.childNodes;
for (let i = 0; i < optionsChildren.length; i++) {
    let childNodes = optionsChildren[i];
    console.log(`Name of the child node: ${childNodes.nodeName}; type of the child node: ${childNodes.nodeType}`);
}
// Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph

// *--нод з таким селектором відсутній, тому нижче код перевіряє чи він є, 
// *--якщо нема - створює біля першого параграфа на сторінці і виділяє зеленим кольором. 
// *--цього нема в завданні, але я не знав, що робити, якщо такий клас відсутній, тому імпровізую :)
let testParagraph = document.querySelector('.testParagraph');
if (testParagraph !== null) {
    testParagraph.innerText = 'This is a paragraph'
} else {
    let newTestParagraph = document.createElement('p');
    newTestParagraph.innerText = 'This is a paragraph';
    newTestParagraph.classList.add('testParagraph');
    document.querySelector('p').append(newTestParagraph);
    document.querySelector('.testParagraph').style.background = 'green';
}

//Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. 
let mainHeaderChildren = document.querySelector('.main-header').children;
console.log(mainHeaderChildren);

//Кожному з елементів присвоїти новий клас nav-item
for (let i = 0; i < mainHeaderChildren.length; i++) {
    let children = mainHeaderChildren[i];
    children.classList.add('nav-item')}

//Знайти всі елементи із класом section-title.
let sectionTitleElems = document.querySelectorAll('.section-title');
console.log(sectionTitleElems);

//Видалити цей клас у цих елементів.
for (let i = 0; i < sectionTitleElems.length; i++) {
    let elements = sectionTitleElems[i];
    elements.classList.remove('section-title');
}


