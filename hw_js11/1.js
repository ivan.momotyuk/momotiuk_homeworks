let iconsOpenEye = Array.from(document.querySelectorAll('.fa-eye'));
let iconsClosedEye = Array.from(document.querySelectorAll('.fa-eye-slash'));
let iconPassword = Array.from(document.querySelectorAll('.icon-password'));
let passwords = Array.from(document.querySelectorAll('[type="password"]'));

iconsOpenEye.map(node => {
    node.style.display = 'none';
})

iconPassword.map(element => element.addEventListener('click', changeVisibility));

//функція для івент лістенера з іконками. Є 4 варіанти куди натискати, кожен з них викликає завжди однакові зміни, 
//тому прописую світч по кліку саме на конкретну іконку для конкретного поля. 
function changeVisibility(event) {
    switch (event.target) {
        case iconsOpenEye[0]:
            iconsOpenEye[0].style.display = 'none';
            iconsClosedEye[0].style.display = 'block';
            passwords[0].type = 'password';
            break;
        case iconsOpenEye[1]:
            iconsOpenEye[1].style.display = 'none';
            iconsClosedEye[1].style.display = 'block';
            passwords[1].type = 'password';
            break;
        case iconsClosedEye[0]:
            iconsOpenEye[0].style.display = 'block';
            iconsClosedEye[0].style.display = 'none';
            passwords[0].type = 'text';
            break;
        case iconsClosedEye[1]:
            iconsOpenEye[1].style.display = 'block';
            iconsClosedEye[1].style.display = 'none';
            passwords[1].type = 'text';
            break;
    }
}
//ДДописав другому раперу айдішку в HTML щоб було простіше викликати його 
//і дописати туди червоний текст. Перевірки, які були б логічними, але їх нема в ТЗ, не додавав. 
let form = document.querySelector('.password-form');
let secondWrapper = document.querySelector('#second-wrapper');

form.addEventListener('submit', isSubmitted);

function isSubmitted(event) {
    //Видаляю червоний текст перед наступним кліком на випадок, якщо наступна дія викликатиме іншу подію. 
    let warningText = document.querySelector('#warning');
    if (warningText) {
        warningText.remove();
    }
    //Перевірка спрацьовує лише якщо пароля зовсім нема в обох полях. На пробіли реагуватиме. 
    if (passwords[0].value === '' && passwords[1].value === '') {
        alert("Ви не ввели пароль");
    } else if (passwords[0].value !== passwords[1].value) {
        secondWrapper.insertAdjacentHTML('beforeend', '<p id="warning" style="color:red">Потрібно ввести однакові паролі!</p>');
    } else {
        passwords.map(item => item.value = '');
        alert("Welcome!");
    }
    event.preventDefault();
}


