const menuButtonIdle = document.querySelector('.navigation-mobile--hidden');
const menuButtonOpen = document.querySelector('.navigation-mobile--open');
const navBar = document.querySelector('.navigation-list');

menuButtonIdle.addEventListener('click', openMenu);
menuButtonOpen.addEventListener('click', closeMenu);
window.addEventListener('resize', hideButton);

function openMenu () {
    if (window.innerWidth < 788) {
    navBar.style = "display:flex";
    menuButtonIdle.style = "display:none";
    menuButtonOpen.style = "display:block";
    } else return
}

function closeMenu () {
    if (window.innerWidth < 788) {
    navBar.style = "display: none";
    menuButtonIdle.style = "display:block";
    menuButtonOpen.style = "display:none";
    } else return
}

function hideButton () {
    console.log(window.innerWidth);
    if (window.innerWidth >= 788) {
        menuButtonIdle.style = "display:none";
        menuButtonOpen.style = "display:none";
        navBar.style = "display:flex";
    } else {
        menuButtonIdle.style = "display:block";
        navBar.style = "display: none";
    }
}
