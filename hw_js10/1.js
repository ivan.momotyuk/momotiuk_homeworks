//створюю змінні, в які записую список з табами та список з описами відповідно
let tabs = document.querySelector('.tabs');
let descriptions = document.querySelector('.tabs-content');

//приховую всі описи за замовчуванням. 
//оскільки звертаюся до дочірніх вузлів, не всі з яких є тегами, додаю перевірку, щоб
//не намагатися додати стиль до неіснуючих тегів
for (let i = 0; i < descriptions.childNodes.length; i++) {
    if (descriptions.childNodes[i].innerHTML !== undefined) {
        descriptions.childNodes[i].style = 'display: none';
    }
}
//створюю івент лістенер
tabs.addEventListener('click', tabOpener);

//створюю функцію для івент лістенера
function tabOpener(event) {
    //приховую всі попередні відкриті вкладки перш, ніж відкривати нову, знов з перевіркою
    for (let i = 0; i < descriptions.childNodes.length; i++) {
        if (descriptions.childNodes[i].innerHTML !== undefined) {
            descriptions.childNodes[i].style = 'display: none';
        }
    }
    //створюю змінну, в яку записую таб з активним класом. Якщо він є, 
    //тобто якщо попередно він був відкритий, видаляю його
    let prevActiveTabs = document.querySelector('.active');
    if (prevActiveTabs) {
        prevActiveTabs.classList.remove('active');
    }

    //створюю змінну, в яку записую куди саме був клік
    let openTab = event.target;

    //створюю змінну, в яку викликаю елемент з ID, який відповідає назві вкладки, 
    //на яку був здійснений клік
    let openDescription = document.querySelector(`#${openTab.innerHTML}`);

    // відображаю попередньо прихований текст
    // змінив стиль, щоб текст був плюс-мінус під табами, а не на всю сторінку
    openDescription.style = 'display: block; width: 50%; margin: auto';
    
    //додаю активний клас до однойменної таби
    openTab.classList.add('active');
}
