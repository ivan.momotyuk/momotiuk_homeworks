
const switchBtn = document.querySelector('#switch');
const themeSelector = document.querySelector('#theme-selector');
switchBtn.addEventListener('click', switchTheme);
window.addEventListener('load', setTheme);

function setTheme() {
    !localStorage.getItem('theme') ? localStorage.setItem('theme', 'dark') :
        localStorage.getItem('theme') === 'dark' ? themeSelector.setAttribute('href', './css/main.css') : themeSelector.setAttribute('href', './css/main-light.css');
}

function switchTheme(event) {
    (localStorage.getItem('theme') === 'dark') ? localStorage.setItem('theme', 'light') : localStorage.setItem('theme', 'dark');
    setTheme();
}

